﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;

namespace Team_Hero_Coder
{

    public class PartyAIManager
    {

        const int CoreStats = 1;
        const int Perk = 2;
        const int StatusEffect = 3;
        const int PassiveAbility = 4;
        const int PartyInventoryItem = 5;
        const int Ability = 6;
        const int FoePartyInfoStart = 7;

        const int CharacterWithInitiative = 8;
        const int PossibleAction = 9;
        const int ActionLegality = 10;
        const int RecordOfBattleAction = 11;
        const int IsFistStateSentForNewBattle = 12;

        public static bool actionFileHasBeenWriten;

        public PartyAIManager()
        {
            FileNames.Init();
            TeamHeroCoder.AbilityID.Init();
            TeamHeroCoder.HeroClassID.Init();
            TeamHeroCoder.PassiveAbilityID.Init();
            TeamHeroCoder.StatusEffectID.Init();
            TeamHeroCoder.ItemID.Init();
            //TeamHeroCoder.BattleActionID.Init();
            TeamHeroCoder.ItemCost.Init();
            TeamHeroCoder.AbilityManaCost.Init();
            TeamHeroCoder.ItemEssenceCost.Init();
            TeamHeroCoder.PerkID.Init();
            TeamHeroCoder.recordOfBattleActions = new LinkedList<RecordOfBattleAction>();


            if (File.Exists(FileNames.battleReport) && File.Exists(FileNames.flag))
            {
                if (!File.Exists(FileNames.battleReport))
                {
                    FileStream f = File.Open(FileNames.battleReport, FileMode.Open);
                    StreamReader sr = new StreamReader(f);

                    string data;
                    while ((data = sr.ReadLine()) != null)
                    {
                        string[] processing = data.Split(",");
                        int identifier = Int32.Parse(processing[0]);

                        TeamHeroCoder.recordOfBattleActions.AddLast(new RecordOfBattleAction(Int32.Parse(processing[1]), Int32.Parse(processing[2]), Int32.Parse(processing[3]), Int32.Parse(processing[4])));
                    }
                    sr.Close();
                    f.Close();
                }
            }

            while (true)
            {

                if (File.Exists(FileNames.error) && File.Exists(FileNames.flag))
                {
                    FileStream f = File.Open(FileNames.error, FileMode.Open);
                    StreamReader sr = new StreamReader(f);

                    string data;
                    while ((data = sr.ReadLine()) != null)
                    {
                        Console.WriteLine("Error: " + data);
                    }

                    sr.Close();
                    f.Close();

                    File.Delete(FileNames.error);

                    Console.WriteLine("Press any key to continue.");
                    Console.ReadKey();


                }
                if (File.Exists(FileNames.battleState) && File.Exists(FileNames.flag))
                {
                    ReadBattleStateSerializationFile();
                    File.Delete(FileNames.battleState);
                    File.Delete(FileNames.flag);

                    PartyAIManager.actionFileHasBeenWriten = false;
                    PartyAI.ProcessAI();
                }

                Thread.Sleep(1000);
            }
        }

        private void ReadBattleStateSerializationFile()
        {

            TeamHeroCoder.BattleState = new BattleState();


            FileStream f = File.OpenRead(FileNames.battleState);

            StreamReader sr = new StreamReader(f);

            bool isReadingForParty = true;
            Hero lastPC = null;
            LegalActionAndRequirments lastAction = null;

            // Console.WriteLine("starting reading ");

            string data;
            while ((data = sr.ReadLine()) != null)
            {
                //Console.WriteLine("reading " + data);
                string[] processing = data.Split(",");
                int identifier = Int32.Parse(processing[0]);

                if (identifier == CoreStats)
                {
                    lastPC = new Hero(Int32.Parse(processing[1]), Int32.Parse(processing[2]), Int32.Parse(processing[3]), Int32.Parse(processing[4]), Int32.Parse(processing[5]), float.Parse(processing[6]), float.Parse(processing[7]), Int32.Parse(processing[8]), Int32.Parse(processing[9]), Int32.Parse(processing[10]), Int32.Parse(processing[11]));
                    if (isReadingForParty)
                        TeamHeroCoder.BattleState.playerHeroes.AddLast(lastPC);
                    else
                        TeamHeroCoder.BattleState.foeHeroes.AddLast(lastPC);
                }
                else if (identifier == FoePartyInfoStart)
                {
                    isReadingForParty = false;
                }
                else if (identifier == Perk)
                {
                    lastPC.perkIDs.AddLast(Int32.Parse(processing[1]));
                }
                else if (identifier == StatusEffect)
                {
                    lastPC.statusEffects.AddLast(new StatusEffect(Int32.Parse(processing[1]), Int32.Parse(processing[2])));
                }
                else if (identifier == PassiveAbility)
                {
                    lastPC.passiveAbilityIDs.AddLast(Int32.Parse(processing[1]));
                }
                else if (identifier == Ability)
                {
                    lastPC.abilityIDs.AddLast(Int32.Parse(processing[1]));
                }
                else if (identifier == PartyInventoryItem)
                {
                    if (isReadingForParty)
                        TeamHeroCoder.BattleState.playerInventory.AddLast(new InventoryItem(Int32.Parse(processing[1]), Int32.Parse(processing[2])));
                    else
                        TeamHeroCoder.BattleState.foeInventory.AddLast(new InventoryItem(Int32.Parse(processing[1]), Int32.Parse(processing[2])));
                }
                else if (identifier == CharacterWithInitiative)
                {
                    TeamHeroCoder.BattleState.heroWithInitiative = TeamHeroCoder.GetHeroFromCharacterIndex(Int32.Parse(processing[1]));
                }
                else if (identifier == PossibleAction)
                {
                    lastAction = new LegalActionAndRequirments(Int32.Parse(processing[1]));
                    TeamHeroCoder.BattleState.legalActions.AddLast(lastAction);
                }
                else if (identifier == ActionLegality)
                {
                    //Console.WriteLine("Adding legality " + Int32.Parse(processing[1]) + " to " + FinalGambit.BattleActionID.lookUp[lastAction.actionID]);
                    lastAction.legalities.AddLast(Int32.Parse(processing[1]));
                }
                else if (identifier == RecordOfBattleAction)
                {
                    TeamHeroCoder.recordOfBattleActions.AddLast(new RecordOfBattleAction(Int32.Parse(processing[1]), Int32.Parse(processing[2]), Int32.Parse(processing[3]), Int32.Parse(processing[4])));

                    FileStream f2;
                    if (!File.Exists(FileNames.battleReport))
                    {
                        //Console.WriteLine("report create");
                        f2 = File.Create(FileNames.battleReport);
                    }
                    else
                    {
                        //Console.WriteLine("report append");
                        f2 = File.Open(FileNames.battleReport, FileMode.Append);
                    }

                    StreamWriter sw = new StreamWriter(f2);

                    //Console.WriteLine("report write = " + data);
                    sw.WriteLine("" + data);
                    sw.Close();

                    f2.Close();

                }
                else if (identifier == IsFistStateSentForNewBattle)
                {
                    TeamHeroCoder.recordOfBattleActions = new LinkedList<RecordOfBattleAction>();
                    File.Delete(FileNames.battleReport);

                }

            }
            sr.Close();

            f.Close();
        }

    }

    public class BattleState
    {
        public LinkedList<Hero> playerHeroes;
        public LinkedList<Hero> foeHeroes;

        public LinkedList<InventoryItem> playerInventory;
        public LinkedList<InventoryItem> foeInventory;

        public Hero heroWithInitiative;
        public LinkedList<LegalActionAndRequirments> legalActions;


        public BattleState()
        {
            playerHeroes = new LinkedList<Hero>();
            foeHeroes = new LinkedList<Hero>();
            playerInventory = new LinkedList<InventoryItem>();
            foeInventory = new LinkedList<InventoryItem>();

            legalActions = new LinkedList<LegalActionAndRequirments>();
            heroWithInitiative = null;
        }
    }

    public class Hero
    {
        public int characterClassID;

        public int health, maxHealth, mana, maxMana, physicalDefense, specialDefense, special, physicalAttack;
        public float initiativePercent, speed;

        public LinkedList<int> abilityIDs;

        public LinkedList<int> perkIDs;
        public LinkedList<StatusEffect> statusEffects;

        public LinkedList<int> passiveAbilityIDs;

        public Hero(int CharacterClassID, int Health, int MaxHealth, int Mana, int MaxMana, float InitiativePercent, float Speed, int PhysicalDefense, int SpecialDefense, int PhysicalAttack, int Special)
        {
            characterClassID = CharacterClassID;

            health = Health;
            mana = Mana;
            maxHealth = MaxHealth;
            maxMana = MaxMana;
            initiativePercent = InitiativePercent;
            speed = Speed;
            physicalDefense = PhysicalDefense;
            specialDefense = SpecialDefense;
            special = Special;
            physicalAttack = PhysicalAttack;

            abilityIDs = new LinkedList<int>();
            perkIDs = new LinkedList<int>();
            passiveAbilityIDs = new LinkedList<int>();
            statusEffects = new LinkedList<StatusEffect>();
        }
    }

    public class InventoryItem
    {
        public int id;
        public int count;

        public InventoryItem(int ID, int Count)
        {
            id = ID;
            count = Count;
        }

    }

    public class StatusEffect
    {
        public int id;
        public int duration;

        public StatusEffect(int ID, int Duration)
        {
            id = ID;
            duration = Duration;
        }
    }

    public class LegalActionAndRequirments
    {
        public int actionID;
        public LinkedList<int> legalities;

        public LegalActionAndRequirments(int ActionID)
        {
            actionID = ActionID;
            legalities = new LinkedList<int>();
        }
    }

    static public class LegalTargetIdentifiers
    {
        public const int NoTargetRequired = 1;
        public const int TargetAlly = 2;
        public const int TargetFoe = 3;
        public const int TargetSlain = 4;

    }

    public class RecordOfBattleAction
    {
        public Hero characterWithInitiative;
        public Hero characterBeingTargeted;
        public Hero characterThatPerformedCover;
        public int abilityID;

        public RecordOfBattleAction(int CharacterWithInitiative, int CharacterBeingTargeted, int CharacterThatPerformedCover, int AbilityID)
        {
            characterWithInitiative = TeamHeroCoder.GetHeroFromCharacterIndex(CharacterWithInitiative);
            characterBeingTargeted = TeamHeroCoder.GetHeroFromCharacterIndex(CharacterBeingTargeted);
            characterThatPerformedCover = TeamHeroCoder.GetHeroFromCharacterIndex(CharacterThatPerformedCover);
            abilityID = AbilityID;
        }
    }

    public static class TeamHeroCoder
    {
        public static BattleState BattleState;
        public static LinkedList<RecordOfBattleAction> recordOfBattleActions;

        static public void PerformHeroAbility(int abilityID, Hero target)
        {

            if (PartyAIManager.actionFileHasBeenWriten)
            {
                Console.WriteLine("PerformHeroAbility() has already been called!");
                return;
            }

            //if (AreBattleActionAndTargetLegal(battleActionID, target))
            {
                FileStream f = File.Create(FileNames.action);

                StreamWriter sw = new StreamWriter(f);

                if (target == null)
                    Console.WriteLine("Target is null!");

                int targetIndex = GetHeroIndexFromHero(target);

                //Console.WriteLine("writing " + battleActionID + "," + targetIndex);

                sw.WriteLine(abilityID + "," + targetIndex);

                sw.Close();
                f.Close();
                f = File.Create(FileNames.flag);
                f.Close();

                PartyAIManager.actionFileHasBeenWriten = true;
            }
        }

        static public int GetHeroIndexFromHero(Hero hero)
        {
            int count = 0;
            if (TeamHeroCoder.BattleState.foeHeroes.Contains(hero))
            {
                foreach (Hero pc2 in TeamHeroCoder.BattleState.foeHeroes)
                {
                    count++;
                    if (pc2 == hero)
                        return -count;
                }
            }
            else if (TeamHeroCoder.BattleState.playerHeroes.Contains(hero))
            {
                foreach (Hero pc2 in TeamHeroCoder.BattleState.playerHeroes)
                {
                    count++;
                    if (pc2 == hero)
                        return count;
                }
            }

            return 0;
        }

        static public Hero GetHeroFromCharacterIndex(int index)
        {
            int count = 0;
            if (index < 0)
            {
                index = -index;
                foreach (Hero pc2 in TeamHeroCoder.BattleState.foeHeroes)
                {
                    count++;
                    if (count == index)
                        return pc2;
                }
            }
            else if (index > 0)
            {
                foreach (Hero pc2 in TeamHeroCoder.BattleState.playerHeroes)
                {
                    count++;
                    if (count == index)
                        return pc2;
                }
            }

            return null;
        }

        static public bool AreAbilityAndTargetLegal(int abilityID, Hero target, bool writeErrorToConsole)
        {

            bool isLegal = true;

            LegalActionAndRequirments actionAndLegalReqs = null;

            foreach (LegalActionAndRequirments legalAction in TeamHeroCoder.BattleState.legalActions)
            {
                if (legalAction.actionID == abilityID)
                {
                    actionAndLegalReqs = legalAction;
                    break;
                }
            }

            if (actionAndLegalReqs == null)
            {
                isLegal = false;

                if (writeErrorToConsole)
                    Console.WriteLine(AbilityID.lookUp[abilityID] + " is not in the list of possible actions!");
            }

            if (actionAndLegalReqs != null)
            {
                if (target == null && !actionAndLegalReqs.legalities.Contains(LegalTargetIdentifiers.NoTargetRequired))
                {
                    if (writeErrorToConsole)
                        Console.WriteLine(AbilityID.lookUp[abilityID] + " requires a target!");
                    isLegal = false;
                }

                if (target != null)
                {
                    if (target.health > 0)
                    {
                        if (actionAndLegalReqs.legalities.Contains(LegalTargetIdentifiers.TargetSlain))
                        {
                            if (writeErrorToConsole)
                                Console.WriteLine(AbilityID.lookUp[abilityID] + " requires a slain target!");
                            isLegal = false;
                        }
                    }
                    else
                    {
                        if (!actionAndLegalReqs.legalities.Contains(LegalTargetIdentifiers.TargetSlain))
                        {
                            if (writeErrorToConsole)
                                Console.WriteLine(AbilityID.lookUp[abilityID] + " cannot target a slain character!");
                            isLegal = false;
                        }
                    }


                    int ind = GetHeroIndexFromHero(target);

                    //Console.WriteLine("ind = " + ind + "  " + actionAndLegalReqs.legalities.Contains(LegalTargetIdentifiers.TargetFoe) + "    " + actionAndLegalReqs.legalities.Contains(LegalTargetIdentifiers.TargetAlly));

                    if (ind > 0)
                    {
                        if (actionAndLegalReqs.legalities.Contains(LegalTargetIdentifiers.TargetFoe))
                        {
                            if (writeErrorToConsole)
                                Console.WriteLine(AbilityID.lookUp[abilityID] + " cannot target an ally, must target a foe!");
                            isLegal = false;
                        }
                    }
                    else if (ind < 0)
                    {
                        if (actionAndLegalReqs.legalities.Contains(LegalTargetIdentifiers.TargetAlly))
                        {
                            if (writeErrorToConsole)
                                Console.WriteLine(AbilityID.lookUp[abilityID] + " cannot target a foe, must target an ally!");
                            isLegal = false;
                        }
                    }
                }

            }

            return isLegal;
        }

        static public class HeroClassID
        {
            public const int Fighter = 0;
            public const int Wizard = 1;
            public const int Cleric = 2;
            public const int Rogue = 3;

            public const int Monk = 4;
            public const int Alchemist = 5;

            static public Dictionary<int, string> lookUp;

            static public void Init()
            {
                lookUp = new Dictionary<int, string>();
                lookUp.Add(HeroClassID.Fighter, "Fighter");
                lookUp.Add(HeroClassID.Wizard, "Wizard");
                lookUp.Add(HeroClassID.Cleric, "Cleric");
                lookUp.Add(HeroClassID.Rogue, "Rogue");
                lookUp.Add(HeroClassID.Monk, "Monk");
                lookUp.Add(HeroClassID.Alchemist, "Alchemist");
            }

        }

        static public class AbilityID
        {
            public const int NoValue = -1;
            public const int Attack = 0;
            public const int Steal = 1;

            public const int Haste = 13;
            public const int Slow = 14;

            public const int Faith = 19;
            public const int Brave = 20;
            public const int Debrave = 21;
            public const int Defaith = 22;

            public const int Petrify = 27;

            public const int AutoLife = 30;
            public const int Doom = 31;

            public const int QuickCleanse = 33;
            public const int Chakra = 34;

            public const int QuickDispel = 132;

            public const int PoisonStrike = 141;

            public const int FlurryOfBlows = 144;

            public const int Potion = 200;
            public const int Ether = 202;
            public const int Elixir = 204;
            public const int MegaElixir = 205;
            public const int Revive = 206;
            public const int SilenceRemedy = 207;
            public const int PoisonRemedy = 208;
            public const int PetrifyRemedy = 210;
            public const int FullRemedy = 211;

            public const int CraftPotion = 300;
            public const int CraftEther = 302;
            public const int CraftElixer = 304;
            public const int CraftMegaElixer = 305;
            public const int CraftRevive = 306;
            public const int CraftSilenceRemedy = 307;
            public const int CraftPoisonRemedy = 308;
            public const int CraftPetrifyRemedy = 310;
            public const int CraftFullRemedy = 311;

            public const int QuickHit = 410;
            public const int StunStrike = 411;

            public const int MagicMissile = 501;
            public const int FlameStrike = 502;
            public const int Fireball = 503;
            public const int Meteor = 505;

            public const int CureLight = 551;
            public const int CureSerious = 552;

            public const int MassHeal = 554;
            public const int Resurrection = 555;
            public const int QuickHeal = 556;

            public const int ManaBurn = 601;
            public const int PoisonNova = 602;
            public const int SilenceStrike = 603;

            public const int Dispel = 701;
            public const int Cleanse = 702;


            static public Dictionary<int, string> lookUp;
            static List<int> offensiveIDs;
            static List<int> requireNoTargetsIDs;

            static List<int> canBeCoveredIDs;

            static public void Init()
            {

                lookUp = new Dictionary<int, string>();
                lookUp.Add(AbilityID.NoValue, "");

                lookUp.Add(AbilityID.Attack, "Attack");
                lookUp.Add(AbilityID.Steal, "Steal");

                lookUp.Add(AbilityID.Haste, "Haste");
                lookUp.Add(AbilityID.Slow, "Slow");


                lookUp.Add(AbilityID.Faith, "Faith");
                lookUp.Add(AbilityID.Brave, "Brave");
                lookUp.Add(AbilityID.Debrave, "Debrave");
                lookUp.Add(AbilityID.Defaith, "Defaith");

                lookUp.Add(AbilityID.Petrify, "Petrify");

                lookUp.Add(AbilityID.AutoLife, "Auto Life");
                lookUp.Add(AbilityID.Doom, "Doom");

                lookUp.Add(AbilityID.QuickCleanse, "Quick Cleanse");
                lookUp.Add(AbilityID.Cleanse, "Cleanse");

                lookUp.Add(AbilityID.Chakra, "Chakra");

                lookUp.Add(AbilityID.MagicMissile, "Magic Missile");
                lookUp.Add(AbilityID.FlameStrike, "Flame Strike");
                lookUp.Add(AbilityID.Fireball, "Fireball");

                lookUp.Add(AbilityID.PoisonNova, "Poison Nova");

                lookUp.Add(AbilityID.Meteor, "Meteor");

                lookUp.Add(AbilityID.ManaBurn, "Mana Burn");

                lookUp.Add(AbilityID.QuickDispel, "Quick Dispel");
                lookUp.Add(AbilityID.Dispel, "Dispel");

                lookUp.Add(AbilityID.CureLight, "Cure Light");
                lookUp.Add(AbilityID.CureSerious, "Cure Serious");
                lookUp.Add(AbilityID.MassHeal, "Mass Heal");
                lookUp.Add(AbilityID.QuickHeal, "Quick Heal");

                lookUp.Add(AbilityID.Resurrection, "Resurrection");

                lookUp.Add(AbilityID.PoisonStrike, "Poison Strike");
                lookUp.Add(AbilityID.FlurryOfBlows, "Flurry Of Blows");

                lookUp.Add(AbilityID.QuickHit, "Quick Hit");
                lookUp.Add(AbilityID.StunStrike, "Stun Strike");

                lookUp.Add(AbilityID.SilenceStrike, "Silence Strike");

                lookUp.Add(AbilityID.Potion, "Potion");
                lookUp.Add(AbilityID.Ether, "Ether");
                lookUp.Add(AbilityID.Elixir, "Elixir");
                lookUp.Add(AbilityID.MegaElixir, "Mega Elixir");
                lookUp.Add(AbilityID.Revive, "Revive");
                lookUp.Add(AbilityID.SilenceRemedy, "Silence Remedy");
                lookUp.Add(AbilityID.PoisonRemedy, "Poison Remedy");
                lookUp.Add(AbilityID.PetrifyRemedy, "Petrify Remedy");
                lookUp.Add(AbilityID.FullRemedy, "Full Remedy");

                lookUp.Add(AbilityID.CraftPotion, "Craft Potion");
                lookUp.Add(AbilityID.CraftEther, "Craft Ether");
                lookUp.Add(AbilityID.CraftElixer, "Craft Elixer");
                lookUp.Add(AbilityID.CraftMegaElixer, "Craft Mega E.");
                lookUp.Add(AbilityID.CraftRevive, "Craft Revive");
                lookUp.Add(AbilityID.CraftSilenceRemedy, "Craft Silence R.");
                lookUp.Add(AbilityID.CraftPoisonRemedy, "Craft Poison R.");
                lookUp.Add(AbilityID.CraftPetrifyRemedy, "Craft Petrify R.");
                lookUp.Add(AbilityID.CraftFullRemedy, "Craft Full R.");


                offensiveIDs = new List<int>(){
                    AbilityID.Attack,
                    AbilityID.Steal,
                    AbilityID.Slow,
                    AbilityID.Debrave,
                    AbilityID.Defaith,

                    AbilityID.Petrify,
                    AbilityID.Doom,

                    AbilityID.MagicMissile,
                    AbilityID.FlameStrike,
                    AbilityID.Fireball,
                    AbilityID.Meteor,
                    AbilityID.ManaBurn,


                    AbilityID.QuickDispel,
                    AbilityID.Dispel,
                    AbilityID.PoisonStrike,

                    AbilityID.FlurryOfBlows,
                    AbilityID.QuickHit,
                    AbilityID.StunStrike,


                    AbilityID.SilenceStrike,
                    AbilityID.PoisonNova,

                    };

                requireNoTargetsIDs = new List<int>(){
                    Steal,

                    Fireball,
                    Meteor,

                    FlurryOfBlows,

                    MegaElixir,
                    CraftPotion,
                    CraftEther,
                    CraftElixer,
                    CraftMegaElixer,
                    CraftRevive,
                    CraftSilenceRemedy,
                    CraftPoisonRemedy,
                    CraftPetrifyRemedy,
                    CraftFullRemedy,

                    MassHeal,

                    PoisonNova
                    };


                canBeCoveredIDs = new List<int>() { Attack, PoisonStrike, QuickHit, StunStrike, SilenceStrike };

            }

            static public bool IsOffensiveID(int abilityID)
            {
                return offensiveIDs.Contains(abilityID);
            }

            static public bool IsBattleActionItem(int abilityID)
            {
                //Debug.Log("checking " + battleActionID);
                string battleActionName = lookUp[abilityID];
                foreach (int i in ItemID.lookUp.Keys)
                {
                    if (ItemID.lookUp[i] == battleActionName)
                    {
                        //Debug.Log(ItemID.lookUp[i] + " == " + battleActionName);
                        return true;
                    }
                }

                return false;
            }

            static public bool HasTargetAsRequirement(int abilityID)
            {
                return !requireNoTargetsIDs.Contains(abilityID);
            }

            static public bool CanBattleActionBeCovered(int abilityID)
            {
                return canBeCoveredIDs.Contains(abilityID);
            }

        }

        static public class ItemID
        {

            public const int Potion = 200;
            public const int Ether = 202;
            public const int Elixir = 204;
            public const int MegaElixir = 205;
            public const int Revive = 206;
            public const int SilenceRemedy = 207;
            public const int PoisonRemedy = 208;
            public const int PetrifyRemedy = 210;
            public const int FullRemedy = 211;
            public const int Essence = 212;

            static public Dictionary<int, string> lookUp;

            static public void Init()
            {
                lookUp = new Dictionary<int, string>();
                lookUp.Add(ItemID.Potion, "Potion");
                lookUp.Add(ItemID.Ether, "Ether");
                lookUp.Add(ItemID.Elixir, "Elixir");
                lookUp.Add(ItemID.MegaElixir, "Mega Elixir");
                lookUp.Add(ItemID.Revive, "Revive");
                lookUp.Add(ItemID.SilenceRemedy, "Silence Remedy");
                lookUp.Add(ItemID.PoisonRemedy, "Poison Remedy");
                lookUp.Add(ItemID.PetrifyRemedy, "Petrify Remedy");
                lookUp.Add(ItemID.FullRemedy, "Full Remedy");
                lookUp.Add(ItemID.Essence, "Essence");
            }

        }

        static public class PerkID
        {
            public const int FighterGuardian = 1001;
            public const int FighterPaladin = 1002;
            public const int FighterSamurai = 1003;

            public const int WizardEvoker = 1101;
            public const int WizardAbjurer = 1102;
            public const int WizardMastery = 1103;

            public const int ClericHealer = 1201;
            public const int ClericEnchanter = 1202;
            public const int ClericMastery = 1203;

            public const int MonkDisciple = 1301;
            public const int MonkNinja = 1302;
            public const int MonkBoundlessFist = 1303;


            public const int RogueAssassin = 1401;
            public const int RogueSwashbuckler = 1402;
            public const int RogueBandit = 1403;


            public const int AlchemistChemist = 1501;
            public const int AlchemistArcanist = 1502;
            public const int AlchemistAetherist = 1503;

            static public Dictionary<int, string> lookUp;

            static public void Init()
            {
                lookUp = new Dictionary<int, string>();

                lookUp.Add(PerkID.FighterGuardian, "Guardian");
                lookUp.Add(PerkID.FighterPaladin, "Paladin");
                lookUp.Add(PerkID.FighterSamurai, "Samurai");

                lookUp.Add(PerkID.WizardEvoker, "Evoker");
                lookUp.Add(PerkID.WizardAbjurer, "Abjurer");
                lookUp.Add(PerkID.WizardMastery, "Mastery");

                lookUp.Add(PerkID.ClericHealer, "Healer");
                lookUp.Add(PerkID.ClericEnchanter, "Enchanter");
                lookUp.Add(PerkID.ClericMastery, "Mastery");

                lookUp.Add(PerkID.MonkDisciple, "Disciple");
                lookUp.Add(PerkID.MonkNinja, "Ninja");
                lookUp.Add(PerkID.MonkBoundlessFist, "Boundless Fist");

                lookUp.Add(PerkID.RogueAssassin, "Assassin");
                lookUp.Add(PerkID.RogueSwashbuckler, "Swashbuckler");
                lookUp.Add(PerkID.RogueBandit, "Bandit");

                lookUp.Add(PerkID.AlchemistChemist, "Chemist");
                lookUp.Add(PerkID.AlchemistArcanist, "Arcanist");
                lookUp.Add(PerkID.AlchemistAetherist, "Aetherist");

            }

        }

        static public class PassiveAbilityID
        {
            public const int Cover = 1;
            public const int ItemJockey = 19;
            public const int StealthAttack = 20;
            public const int Larceny = 23;
            public const int BitterBloom = 31;
            public const int Adrenaline = 32;


            static public Dictionary<int, string> lookUp;

            static public void Init()
            {
                lookUp = new Dictionary<int, string>();
                lookUp.Add(PassiveAbilityID.Cover, "Cover");

                lookUp.Add(PassiveAbilityID.ItemJockey, "Item Jockey");
                lookUp.Add(PassiveAbilityID.StealthAttack, "Stealth");
                lookUp.Add(PassiveAbilityID.Larceny, "Larceny");

                lookUp.Add(PassiveAbilityID.BitterBloom, "Bitter Bloom");
                lookUp.Add(PassiveAbilityID.Adrenaline, "Adrenaline");

            }

        }

        static public class StatusEffectID
        {
            public const int Poison = 1;

            public const int Haste = 4;
            public const int Slow = 5;

            public const int Faith = 9;
            public const int Brave = 10;
            public const int Debrave = 11;
            public const int Defaith = 12;

            public const int Silence = 15;
            public const int Petrifying = 17;
            public const int Petrified = 18;
            public const int AutoLife = 20;
            public const int Doom = 21;


            static public Dictionary<int, string> lookUp;

            static public void Init()
            {
                lookUp = new Dictionary<int, string>();
                lookUp.Add(StatusEffectID.Poison, "Poison");
                lookUp.Add(StatusEffectID.Haste, "Haste");
                lookUp.Add(StatusEffectID.Slow, "Slow");
                lookUp.Add(StatusEffectID.Faith, "Faith");
                lookUp.Add(StatusEffectID.Brave, "Brave");
                lookUp.Add(StatusEffectID.Debrave, "Debrave");
                lookUp.Add(StatusEffectID.Defaith, "Defaith");
                lookUp.Add(StatusEffectID.Silence, "Silence");
                lookUp.Add(StatusEffectID.Petrifying, "Petrifying");
                lookUp.Add(StatusEffectID.Petrified, "Petrified");
                lookUp.Add(StatusEffectID.AutoLife, "AutoLife");
            }

        }

        public static class ItemCost
        {
            public const int Potion = 10;
            public const int Ether = 15;
            public const int Elixir = 20;
            public const int MegaElixir = 50;
            public const int Revive = 15;
            public const int SilenceRemedy = 2;
            public const int PoisonRemedy = 1;
            public const int PetrifyRemedy = 3;
            public const int FullRemedy = 5;
            public const int Essence = 1;

            static public Dictionary<int, int> lookUpCostValueFromID;
            static public void Init()
            {
                lookUpCostValueFromID = new Dictionary<int, int>();

                lookUpCostValueFromID.Add(ItemID.Potion, Potion);
                lookUpCostValueFromID.Add(ItemID.Ether, Ether);
                lookUpCostValueFromID.Add(ItemID.Elixir, Elixir);
                lookUpCostValueFromID.Add(ItemID.MegaElixir, MegaElixir);
                lookUpCostValueFromID.Add(ItemID.Revive, Revive);
                lookUpCostValueFromID.Add(ItemID.SilenceRemedy, SilenceRemedy);
                lookUpCostValueFromID.Add(ItemID.PoisonRemedy, PoisonRemedy);
                lookUpCostValueFromID.Add(ItemID.PetrifyRemedy, PetrifyRemedy);
                lookUpCostValueFromID.Add(ItemID.FullRemedy, FullRemedy);
                lookUpCostValueFromID.Add(ItemID.Essence, Essence);
            }
        }

        public static class AbilityManaCost
        {
            static Dictionary<int, int> lookup;

            public static void Init()
            {
                lookup = new Dictionary<int, int>();


                lookup.Add(AbilityID.Slow, 15);
                lookup.Add(AbilityID.MagicMissile, 10);
                lookup.Add(AbilityID.PoisonNova, 15);
                lookup.Add(AbilityID.Petrify, 15);
                lookup.Add(AbilityID.FlameStrike, 30);
                lookup.Add(AbilityID.Fireball, 25);
                lookup.Add(AbilityID.ManaBurn, 20);
                lookup.Add(AbilityID.Meteor, 60);

                lookup.Add(AbilityID.CureLight, 10);
                lookup.Add(AbilityID.CureSerious, 20);
                lookup.Add(AbilityID.QuickHeal, 15);

                lookup.Add(AbilityID.MassHeal, 20);
                lookup.Add(AbilityID.Resurrection, 25);
                lookup.Add(AbilityID.Haste, 15);
                lookup.Add(AbilityID.Faith, 15);
                lookup.Add(AbilityID.Brave, 15);
                lookup.Add(AbilityID.Defaith, 10);
                lookup.Add(AbilityID.Debrave, 10);
                lookup.Add(AbilityID.AutoLife, 25);
                lookup.Add(AbilityID.Doom, 15);

                lookup.Add(AbilityID.QuickCleanse, 10);
                lookup.Add(AbilityID.QuickDispel, 10);

                lookup.Add(AbilityID.Cleanse, 15);
                lookup.Add(AbilityID.Dispel, 15);
                lookup.Add(AbilityID.PoisonStrike, 15);
                lookup.Add(AbilityID.FlurryOfBlows, 15);
                lookup.Add(AbilityID.QuickHit, 15);
                lookup.Add(AbilityID.StunStrike, 15);

                lookup.Add(AbilityID.SilenceStrike, 15);
                lookup.Add(AbilityID.Chakra, 0);
                lookup.Add(AbilityID.CraftPotion, 10);
                lookup.Add(AbilityID.CraftEther, 10);
                lookup.Add(AbilityID.CraftElixer, 20);
                lookup.Add(AbilityID.CraftMegaElixer, 25);
                lookup.Add(AbilityID.CraftRevive, 10);
                lookup.Add(AbilityID.CraftFullRemedy, 10);
                lookup.Add(AbilityID.CraftSilenceRemedy, 10);
                lookup.Add(AbilityID.CraftPoisonRemedy, 10);
                lookup.Add(AbilityID.CraftPetrifyRemedy, 10);

            }

            public static int GetManaCostForAbility(int id)
            {
                if (lookup.ContainsKey(id))
                    return lookup[id];
                return 0;
            }

            public static int GetMaximumMPCostForBattleAction(int id)
            {
                if (AbilityID.Chakra == id)
                    return BattleActionParams.ChakraMaxMPReduction;
                return 0;
            }
        }

        public static class ItemEssenceCost
        {
            static Dictionary<int, int> lookup;

            public static void Init()
            {
                lookup = new Dictionary<int, int>();

                lookup.Add(AbilityID.CraftPotion, 2);
                lookup.Add(AbilityID.CraftEther, 2);
                lookup.Add(AbilityID.CraftElixer, 3);
                lookup.Add(AbilityID.CraftMegaElixer, 4);
                lookup.Add(AbilityID.CraftRevive, 2);
                lookup.Add(AbilityID.CraftSilenceRemedy, 2);
                lookup.Add(AbilityID.CraftPoisonRemedy, 2);
                lookup.Add(AbilityID.CraftPetrifyRemedy, 2);
                lookup.Add(AbilityID.CraftFullRemedy, 2);
            }

            public static int GetEssenceCostForItem(int id)
            {
                if (lookup.ContainsKey(id))
                    return lookup[id];
                return 0;
            }

        }


        static public int CalculateDamageAmount(Hero attacker, float attackModifier, Hero defender, bool isPhysical)
        {

            int attack, defense;
            float damagePercentMod = 1;

            if (isPhysical)
            {
                attack = attacker.physicalAttack;
                defense = defender.physicalDefense;

                if (HasStatusEffectID(attacker.statusEffects, StatusEffectID.Brave))
                    damagePercentMod += StatusEffectParams.BravePercentMod;

                if (HasStatusEffectID(attacker.statusEffects, StatusEffectID.Debrave))
                    damagePercentMod += StatusEffectParams.DebravePercentMod;
            }
            else
            {
                attack = attacker.special;
                defense = defender.specialDefense;


                if (HasStatusEffectID(attacker.statusEffects, StatusEffectID.Faith))
                    damagePercentMod += StatusEffectParams.FaithPercentMod;

                if (HasStatusEffectID(attacker.statusEffects, StatusEffectID.Defaith))
                    damagePercentMod += StatusEffectParams.DefaithPercentMod;
            }

            float baseDamageAmount = (float)attack * attackModifier;
            float reductionAmount = 1f - (float)defense / 100f;
            int damageAmount = (int)(baseDamageAmount * reductionAmount * damagePercentMod);

            if (damageAmount < 0)
                damageAmount = 0;

            return damageAmount;

        }
        static public int CalculateHealingAmount(Hero healer, float modifier)
        {
            int magicAttack = healer.special;
            float healPercentMod = 1;

            if (HasStatusEffectID(healer.statusEffects, StatusEffectID.Faith))
                healPercentMod += StatusEffectParams.FaithPercentMod;

            if (HasStatusEffectID(healer.statusEffects, StatusEffectID.Defaith))
                healPercentMod += StatusEffectParams.DefaithPercentMod;

            int amount = (int)(magicAttack * modifier * healPercentMod);

            if (amount < 0)
                amount = 0;

            return amount;
        }


        static private bool HasStatusEffectID(LinkedList<StatusEffect> statusEffects, int id)
        {
            foreach (StatusEffect se in statusEffects)
            {
                if (se.id == id)
                    return true;
            }

            return false;
        }


        // static public int CalculateDamage(int id, PartyCharacter attacker, PartyCharacter target)
        // {
        //     int attack = 0;
        //     float attackModifier = 0;
        //     int defense = 0;
        //     bool isPhysical = false;

        //     if (id == BattleActionID.Attack || id == BattleActionID.FlurryOfBlows || id == BattleActionID.PoisonStrike || id == BattleActionID.QuickHit || id == BattleActionID.SilenceStrike || id == BattleActionID.StunStrike)
        //         isPhysical = true;

        //     if (isPhysical)
        //     {
        //         attack = attacker.strength;
        //         defense = target.defense;
        //     }
        //     else
        //     {
        //         attack = attacker.wisdom;
        //         defense = target.magicDefense;
        //     }

        //     if (id == BattleActionID.Attack)
        //         attackModifier = BattleActionParams.AttackMod;
        //     else if (id == BattleActionID.Fireball)
        //         attackModifier = BattleActionParams.FireballMod;
        //     else if (id == BattleActionID.FlameStrike)
        //         attackModifier = BattleActionParams.FlameStrikeMod;
        //     else if (id == BattleActionID.FlurryOfBlows)
        //         attackModifier = BattleActionParams.FlurryOfBlowsMod;
        //     else if (id == BattleActionID.MagicMissile)
        //         attackModifier = BattleActionParams.MagicMissileMod;
        //     else if (id == BattleActionID.Meteor)
        //         attackModifier = BattleActionParams.MeteorMod;
        //     else if (id == BattleActionID.PoisonStrike)
        //         attackModifier = BattleActionParams.PoisonStrikeMod;
        //     else if (id == BattleActionID.QuickHit)
        //         attackModifier = BattleActionParams.QucikHitDamageMod;
        //     else if (id == BattleActionID.SilenceStrike)
        //         attackModifier = BattleActionParams.SilenceStrikeDamageMod;
        //     else if (id == BattleActionID.StunStrike)
        //         attackModifier = BattleActionParams.StunStrikeDamageMod;
        //     else
        //         Console.WriteLine("Attack ID not found in CalculateDamage(), damage modifier is set to 0.");

        //     float baseDamageAmount = (float)attack * attackModifier;
        //     float reductionAmount = 1f - (float)defense / 1000f;
        //     int damageAmount = (int)(baseDamageAmount * reductionAmount);

        //     if (damageAmount < 0)
        //         damageAmount = 0;

        //     return damageAmount;
        // }

        // static public int CalculateHeal(int id, PartyCharacter healer)
        // {
        //     int wisdom = healer.wisdom;
        //     float modifier = 0;
        //     bool isItem = false;


        //     if (id == BattleActionID.CureLight)
        //         modifier = BattleActionParams.CureLightMod;
        //     else if (id == BattleActionID.CureSerious)
        //         modifier = BattleActionParams.CureSeriousMod;
        //     else if (id == BattleActionID.MassHeal)
        //         modifier = BattleActionParams.MassHealMod;
        //     else if (id == BattleActionID.Resurrection)
        //         modifier = BattleActionParams.ResurrectionMod;

        //     else if (id == BattleActionID.Potion)
        //     {
        //         modifier = BattleActionParams.PotionHealAmount;
        //         isItem = true;
        //     }
        //     else if (id == BattleActionID.Elixer)
        //     {
        //         modifier = BattleActionParams.ElixerHealAmount;
        //         isItem = true;
        //     }
        //     else if (id == BattleActionID.MegaElixer)
        //     {
        //         modifier = BattleActionParams.MegaElixerHealAmount;
        //         isItem = true;
        //     }
        //     else if (id == BattleActionID.Revive)
        //     {
        //         modifier = BattleActionParams.PotionHealAmount;
        //         isItem = true;
        //     }
        //     else
        //         Console.WriteLine("Attack ID not found in CalculateHeal(), heal modifier is set to 0.");

        //     int amount;

        //     if (!isItem)
        //         amount = (int)(((float)wisdom) * modifier);
        //     else
        //         amount = (int)modifier;

        //     if (amount < 0)
        //         amount = 0;

        //     return amount;
        // }

        // static public int CalculateManaRestore(int id, PartyCharacter healer)
        // {

        //     int wisdom = healer.wisdom;
        //     float modifier = 0;
        //     bool isItem = false;


        //     if (id == BattleActionID.Chakra)
        //         modifier = BattleActionParams.ChakraMod;
        //     else if (id == BattleActionID.Ether)
        //     {
        //         modifier = BattleActionParams.EtherMPHealAmount;
        //         isItem = true;
        //     }
        //     else if (id == BattleActionID.Elixer)
        //     {
        //         modifier = BattleActionParams.ElixerMPHealAmount;
        //         isItem = true;
        //     }
        //     else if (id == BattleActionID.MegaElixer)
        //     {
        //         modifier = BattleActionParams.MegaElixerMPHealAmount;
        //         isItem = true;
        //     }
        //     else
        //         Console.WriteLine("Action ID not found in CalculateManaRestore(), mana restore modifier is set to 0.");

        //     int amount;

        //     if (!isItem)
        //         amount = (int)(((float)wisdom) * modifier);
        //     else
        //         amount = (int)modifier;

        //     if (amount < 0)
        //         amount = 0;

        //     return amount;
        // }

        // static public int CalculateManaDamage(int id, PartyCharacter attacker, PartyCharacter target)
        // {
        //     int attack = 0;
        //     float attackModifier = 0;
        //     int defense = 0;
        //     bool isPhysical = false;

        //     if (id == BattleActionID.Attack || id == BattleActionID.FlurryOfBlows || id == BattleActionID.PoisonStrike || id == BattleActionID.QuickHit || id == BattleActionID.SilenceStrike || id == BattleActionID.StunStrike)
        //         isPhysical = true;

        //     if (isPhysical)
        //     {
        //         attack = attacker.strength;
        //         defense = target.defense;
        //     }
        //     else
        //     {
        //         attack = attacker.wisdom;
        //         defense = target.magicDefense;
        //     }

        //     if (id == BattleActionID.ManaBurn)
        //         attackModifier = BattleActionParams.ManaBurnMpDamageMod;
        //     else
        //         Console.WriteLine("Attack ID not found in CalculateManaDamage(), mana damage modifier is set to 0.");

        //     float baseDamageAmount = (float)attack * attackModifier;
        //     float reductionAmount = 1f - (float)defense / 1000f;
        //     int damageAmount = (int)(baseDamageAmount * reductionAmount);

        //     if (damageAmount < 0)
        //         damageAmount = 0;

        //     return damageAmount;
        // }

        static public class BattleActionParams
        {

            public const float AttackDamageMod = 10f;

            public const float MagicMissileDamageMod = 10f;

            public const float FlameStrikeDamageMod = 15f;
            public const float FireballDamageMod = 9f;

            public const float MeteorDamageMod = 12f;


            public const float CureLightHealMod = 15f;
            public const float CureSeriousHealMod = 22.5f;
            public const float MassHealHealMod = 9f;
            public const float ResurrectionHealMod = 20f;
            public const float QuickHealHealMod = 15f;
            public const float QuickHealInitiativeReplenishPercent = 0.3f;


            public const float PoisonStrikeDamageMod = 8f;

            public const float FlurryOfBlowsDamageMod = 6f;

            public const float ChakraManaReplenishMod = 2f;
            public const int ChakraMaxMPReduction = -10;


            public const int FullHealAmount = 9999;

            public const float ReviveHealPercent = 0.5f;


            public const int PotionCraftCount = 2;
            public const int EtherCraftCount = 1;
            public const int ElixirCraftCount = 1;
            public const int MegaElixirCraftCount = 1;
            public const int ReviveCraftCount = 2;
            public const int SilenceRemedyCraftCount = 3;
            public const int PoisonRemedyCraftCount = 3;

            public const int PetrifyRemedyCraftCount = 3;
            public const int FullRemedyCraftCount = 1;


            public const float QucikHitInitiativeReplenish = 0.5f;
            public const float QucikHitDamageMod = 10f;


            public const float StunStrikeInitiativeReductionPercent = 0.4f;
            public const float StunStrikeDamageMod = 10f;

            public const float ManaBurnManaDamageMod = 2f;


            public const float SilenceStrikeDamageMod = 4f;


            public const float ItemCraftingInitiativeReplenish = 0.5f;

            public const float QuickCleanseInitiativeReplenish = 0.5f;

            public const float QuickDispelInitiativeReplenish = 0.5f;

            public const float SilenceRemedyInitiativeReplenish = 0.5f;
            public const float PoisonRemedyInitiativeReplenish = 0.5f;

        }

        static public class StatusEffectParams
        {
            public const int HasteDuration = 4;
            public const int SlowDuration = 4;

            public const float HasteSpeedMod = 1.5f;
            public const float SlowSpeedMod = 0.5f;

            public const int PoisonDuration = 4;
            public const int PoisonTickDamage = -140;


            public const int BraveDuration = 4;
            public const int DebraveDuration = 4;
            public const int FaithDuration = 4;
            public const int DefaithDuration = 4;

            public const int PetrificationTime = 3;
            public const int DoomTime = 3;

            public const float FaithPercentMod = 0.5f;
            public const float DefaithPercentMod = -0.5f;
            public const float BravePercentMod = 0.5f;
            public const float DebravePercentMod = -0.5f;

            public const int SilenceStrikeDuraction = 1;
            public const int AutoLifeDuration = 25;


        }

        static public class PassiveEffectParams
        {
            public const float ItemJockeyInitiativeRefill = 0.4f;

            public const int LarcenyProcIfUnder = 5;

            public const float AdrenalineDamagePercentBuff = 0.5f;

            public const float AdrenalineProcIfHealthPercentLowerThan = 0.51f;

            public const float BitterBloomExtraDamagePercent = 0.10f;

        }


    }

    public static class FileNames
    {

        public static string action;
        public static string flag;
        public static string battleReport;
        public static string error;
        public static string battleState;

        public static void Init()
        {
            Console.WriteLine("Exchange Folder Path: " + FileExchangePath.exchangePath);

            //string[] paths = { FileExchangePath.exchangePath, "Action.txt"};
            //action = Path.Combine(paths);

            //paths = new string[] { FileExchangePath.exchangePath, "BattleReport.txt" };
            //battleReport = Path.Combine(paths);

            //paths = new string[] { FileExchangePath.exchangePath, "Flag.txt" };
            //flag = Path.Combine(paths);

            //paths = new string[] { FileExchangePath.exchangePath, "Error.txt" };
            //error = Path.Combine(paths);

            //paths = new string[] { FileExchangePath.exchangePath, "BattleState.txt" };
            //battleState = Path.Combine(paths);

            action = FileExchangePath.exchangePath + Path.DirectorySeparatorChar + "Action.txt";

            battleReport = FileExchangePath.exchangePath + Path.DirectorySeparatorChar + "BattleReport.txt";
            flag = FileExchangePath.exchangePath + Path.DirectorySeparatorChar + "Flag.txt";
            error = FileExchangePath.exchangePath + Path.DirectorySeparatorChar + "Error.txt";
            battleState = FileExchangePath.exchangePath + Path.DirectorySeparatorChar + "BattleState.txt";


            Console.WriteLine("looking for battle state in: " + battleState);


        }

    }

}

